5.times do
  Book.create(
    {
      title:  Faker::Book.title,
      author: Faker::Movies::StarWars.character
    }
  )
end
